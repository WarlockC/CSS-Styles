## [Gitea Modern](https://codeberg.org/Freeplay/CSS-Styles/src/branch/main/Gitea#gitea-modern)
| ![Gitea Modern Repository Page](https://codeberg.org/Freeplay/css-styles/raw/branch/main/images/codeberg-modern_repoPg.png) | ![Gitea Modern Explore Page](https://codeberg.org/Freeplay/css-styles/raw/branch/main/images/codeberg-modern_explorePg.png) | ![Gitea Modern Profile Page](https://codeberg.org/Freeplay/CSS-Styles/raw/branch/main/images/codeberg-modern_profilePg.png)
|---|---|---|

---

#### Click into folders to view styles for that site.

| [Icon from Lucide](https://lucide.dev/) | [**View UserStyles list**](https://codeberg.org/Freeplay/UserStyles) | `.user.css` files can be applied using the [Stylus](https://add0n.com/stylus.html) browser extension. |
|---|---|---|